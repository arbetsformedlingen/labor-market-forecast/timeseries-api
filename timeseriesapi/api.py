from flask_restx import Resource, Api, reqparse, abort, Namespace
from timeseriesapi.opensearch_store import OpensearchStore
import time
import logging
from opensearchpy import RequestError
from timeseriesapi import settings
from timeseriesapi.model.pagination_params import PaginationParams
from timeseriesapi.model.search_prediction_query_params import SearchPredictionQueryParams

version_number = f'{settings.API_VERSION} {settings.API_VERSION_STATUS}'

api = Api(title='Labour market forecast API', default='Labour-Market-Forecast-API',
          version=version_number,
          description='Endpoints for searching labour market forecasts')

ns_api_info = Namespace('Namespace for api info')
api.add_namespace(ns_api_info, '/')

ns_search_occupations = Namespace('Search occupation forecasts',
                                 description='Search for occupation forecasts')

api.add_namespace(ns_search_occupations, '/')

ns_match_occupations = Namespace('Match occupations',
                                 description='Match related occupations by for example an education or by competence terms in a text')

log = logging.getLogger(__name__)
opensearch_store = OpensearchStore()

endpoint_responses_type_1 = {
    200: 'OK',
    400: 'Bad request',
    500: 'Technical error'
}

endpoint_responses_type_2 = {
    200: 'OK',
    400: 'Bad request',
    404: 'Not found',
    500: 'Technical error'
}

HELP_OFFSET_PARAM = 'The offset parameter defines the index for the first result item you want to fetch.'

## Endpoint that returns info about api
@ns_api_info.route('api_info')
@ns_api_info.hide
class ApiInfo(Resource):
    @api.doc()
    def get(self):
        return {"api_name": "Labour market forecast API", "api_version": settings.API_VERSION, "api_released": settings.API_VERSION_RELEASED,
                "api_documentation": "",
                "api_status": settings.API_VERSION_STATUS}

@ns_search_occupations.route('v1/occupations')
class OccupationForecastListV1(Resource):
    FORECAST_LIST_DEFAULT_LIMIT = 10
    occupation_forecast_list_parser = reqparse.RequestParser()
    occupation_forecast_list_parser.add_argument('query', type=str,
                                       help='Free text search if provided. Otherwise retrieve all educations.')
    occupation_forecast_list_parser.add_argument('limit', type=int, help='Number of results to fetch. Default value is %s.' % FORECAST_LIST_DEFAULT_LIMIT)
    occupation_forecast_list_parser.add_argument('offset', type=int,
                                                 help=HELP_OFFSET_PARAM)

    @api.doc(description='Endpoint for searching occupation forecasts.',
             parser=occupation_forecast_list_parser,
             responses=endpoint_responses_type_1)
    def get(self):
        start_time = int(time.time() * 1000)
        args = self.occupation_forecast_list_parser.parse_args()

        search_prediction_query_params = SearchPredictionQueryParams()
        pagination_params =  PaginationParams()

        if args.get('query', None):
            search_prediction_query_params.free_text = args['query']

        if not args.get('limit'):
            pagination_params.limit = self.FORECAST_LIST_DEFAULT_LIMIT
        if args.get('limit'):
            pagination_params.limit = args['limit']
        if args.get('offset'):
            pagination_params.offset = args['offset']

        full_search = True
        if search_prediction_query_params:
            full_search = False

        # If search string (i.e. not full search):
        if full_search:
            result = self.find_by_full_search_pagination(pagination_params)
        # If empty search string (i.e. full search) and pagination:
        else:
            result = self.find_by_freetext(search_prediction_query_params, pagination_params)

        log.debug(f"(v1 get educations) Took: {int(time.time() * 1000 - start_time)} milliseconds")
        return result

    def find_by_full_search_pagination(self, pagination_params):
        # Note limitation that it's only possible to fetch first 10000 educations...
        try:
            return opensearch_store.full_search(pagination_params)
        except RequestError:
            abort(400, custom='Wrong input')

    def find_by_freetext(self, search_prediction_query_params: SearchPredictionQueryParams, pagination_params: PaginationParams):
        # Note limitation that it's only possible to fetch first 10000 educations
        # (probably not a problem when proving search string to match)...
        try:
            return opensearch_store.freetext_search(search_prediction_query_params, pagination_params,
                                                    True)
        except RequestError:
            abort(400, custom='Wrong input')
