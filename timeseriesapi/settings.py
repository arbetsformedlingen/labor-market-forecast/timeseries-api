import os

ES_HOST = os.getenv("ES_HOST", "127.0.0.1")
ES_PORT = os.getenv("ES_PORT", 9200)
ES_USER = os.getenv("ES_USER", "admin")
ES_PWD = os.getenv("ES_PWD", "admin")
ES_USE_SSL = os.getenv('ES_USE_SSL', 'true').lower() == 'true'
ES_VERIFY_CERTS = os.getenv('ES_VERIFY_CERTS', 'true').lower() == 'true'

ES_OCCUPATION_PREDICTIONS_ALIAS = os.getenv('ES_OCCUPATION_PREDICTIONS_ALIAS', 'occupation-predictions')

API_VERSION = '0.0.1'
API_VERSION_STATUS = ''
API_VERSION_RELEASED = '2024-01-11'