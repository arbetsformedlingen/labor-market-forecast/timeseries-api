class PaginationParams(object):
    def __init__(self):
        self._limit = None
        self._offset = None

    @property
    def limit(self):
        # Getter
        return self._limit

    @limit.setter
    def limit(self, limit):
        # setter
        self._limit = limit


    @property
    def offset(self):
        # Getter
        return self._offset

    @offset.setter
    def offset(self, offset):
        # setter
        self._offset = offset