class SearchPredictionQueryParams(object):
    def __init__(self):
        self._free_text = None

    @property
    def free_text(self):
        # Getter
        return self._free_text

    @free_text.setter
    def free_text(self, free_text):
        # setter
        self._free_text = free_text

# c = C()
# c.x = 'foo'  # setter called
# foo = c.x    # getter called