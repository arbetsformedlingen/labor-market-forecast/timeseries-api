#import itertools
import json
import time
import logging
from timeseriesapi import settings
from timeseriesapi.model.search_prediction_query_params import SearchPredictionQueryParams
from timeseriesapi.model.pagination_params import PaginationParams
from timeseriesapi.repository import opensearch_client


class OpensearchStore:

    def __init__(self, os_client=None):
        logging.basicConfig(level=logging.INFO)
        self.log = logging.getLogger(__name__)
        if os_client:
            self.os_client = os_client
        else:
            self.os_client = None

    def find_by_id(self, id_value):
        index = settings.ES_OCCUPATION_PREDICTIONS_ALIAS
        query_dsl = {
            "query": {"ids": {"values": [id_value]}}
        }
        return self.find(query_dsl, index)

    # TODO limit of max 10000 docs in result:
    def full_search(self, pagination_params: PaginationParams):
        index = settings.ES_OCCUPATION_PREDICTIONS_ALIAS
        query_dsl = {
            "query": {
                "match_all": {}
            },
            "track_total_hits": True,
            "track_scores": True
        }

        if pagination_params.limit:
            query_dsl['size'] = pagination_params['limit']
        else:
            query_dsl['size'] = 2000
        if pagination_params.offset:
            query_dsl['from'] = pagination_params.offset
        self.log.debug(f"Query DSL: {query_dsl}")
        return self.search(query_dsl, index)

    # Returns all docs in the index using scan operator.
    def full_scan_search(self, exclude_events_and_providers=True):
        index = settings.ES_OCCUPATION_PREDICTIONS_ALIAS
        query_dsl = {
            "query": {
                "match_all": {}
            },
            "track_total_hits": True,
            "track_scores": True
        }
        if exclude_events_and_providers:
            query_dsl['_source'] = {"exclude": ["events", "education_providers"]}
        self.log.debug(f"Query DSL: {query_dsl}")
        scan_result = opensearch_client.scan_search(query_dsl, index, os_client=self.os_client)
        counter = 0
        yield '['
        for result_doc in scan_result:
            if counter > 0:
                yield ','
            yield json.dumps(result_doc)
            counter += 1
        self.log.info(f"(full_scan_search)Delivered: {counter} educations")
        yield ']'

    def freetext_search(self, search_prediction_query_params: SearchPredictionQueryParams,
                        pagination_params: PaginationParams,
                        exclude_events_and_providers):

        index = settings.ES_OCCUPATION_PREDICTIONS_ALIAS
        query_dsl = {
            "query": {
                "bool": {
                }
            },
            "track_total_hits": True,
            "track_scores": True
        }
        # Free text search...
        if search_prediction_query_params.free_text:
            free_text =  search_prediction_query_params.free_text
            query_dsl['query']['bool']['should'] = [
                            {
                                "regexp": {
                                    "occupation": {
                                        "value": free_text + ".*",
                                        "boost": 10
                                    }
                                }
                            },
                        ]
            query_dsl['query']['bool']['minimum_should_match'] = 1
            query_dsl['query']['bool']['boost'] = 1.0

        must_list = []

        if len(must_list) > 0:
            query_dsl['query']['bool']['must'] = must_list

        if pagination_params.limit:
            query_dsl['size'] = pagination_params.limit
        else:
            query_dsl['size'] = 10000
        if pagination_params.offset:
            query_dsl['from'] = pagination_params.offset
        self.log.debug(f"Query DSL: {json.dumps(query_dsl)}")
        return self.search(query_dsl, index)

    # Search where exactly one hit is expected (e.g search by id)...
    def find(self, query_dsl, index):
        query_result = opensearch_client.search(query_dsl, index, os_client=self.os_client)
        formatted_result = None
        if query_result.get('hits', None):
            hits = query_result['hits']
            if hits.get('hits', None):
                hits_result = hits['hits']
                if len(hits_result) > 0:
                    formatted_result = {}
                    doc = hits_result[0]
                    if doc.get('_source', None):
                        formatted_result = doc['_source']
        return formatted_result

    # Search where zero or more hits are expected...
    def search(self, query_dsl, index):
        query_result = opensearch_client.search(query_dsl, index, os_client=self.os_client)
        total_value = 0
        formatted_result = None
        if query_result.get('hits', None):
            hits = query_result['hits']
            if hits.get('total', None):
                total = hits['total']
                if total.get('value', None):
                    total_value = total['value']
            if hits.get('hits', None):
                hits_result = hits['hits']
                if len(hits_result) > 0:
                    formatted_result = []
                    for doc in hits_result:
                        if doc.get('_source', None):
                            formatted_result.append(doc['_source'])

        result = {'hits': total_value, 'result': formatted_result}
        return result

    def calculate_time(self, start_time, len_ads):
        elapsed_time = time.time() - start_time
        m, s = divmod(elapsed_time, 60)
        self.log.info(f"Imported {len_ads} docs in: {int(m)} minutes {int(s)} seconds.")
